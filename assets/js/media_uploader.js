jQuery(document).ready(function($){
  var _custom_media = true,
  _orig_send_attachment = wp.media.editor.send.attachment;
 
  $('.uploader .button').click(function(e) {
    var send_attachment_bkp = wp.media.editor.send.attachment;
    var button = $(this);
    var id = button.attr('id').replace('_button', '');
    _custom_media = true;
    wp.media.editor.send.attachment = function(props, attachment){
      if ( _custom_media ) {
        $("#"+id).val(attachment.url);
      } else {
        return _orig_send_attachment.apply( this, [props, attachment] );
      };
    }
 
    wp.media.editor.open(button);
    return false;
  });

  var uploaders = $('.multi-uploader');
  var remove_item = function(e) {
    e.preventDefault();
    var el = $(this).parent();
    el.remove();
  }

  uploaders.each(function(i,el){
    var list = $(el).find('ul').first();
    $("ul, li").disableSelection();
    list.sortable({
      revert: false
    });

    list.children().each(function(i,el) {
      $(el).find('button').on('click', remove_item);
    });
  });

  $('.multi-uploader .button').click(function(e){
    var send_attachment_bkp = wp.media.editor.send.attachment;
    var button = $(this);
    var id = button.attr('id').replace('_button', '');

    var images = button.parent().find('.images').first(); 

    _custom_media = true;
    wp.media.editor.send.attachment = function(props, attachment){
      if ( _custom_media ) {
        var container = document.createElement('li');
        container = $(container);
        container.addClass('media');

        var close = document.createElement('button');
        close.innerHTML = 'x';
        $(close).on('click', remove_item);

        var input = document.createElement('input');
        input.type = 'hidden';
        input.name = id+'[]';
        input.value = attachment.id;

        var img = document.createElement('img');
        img.src = attachment.sizes.thumbnail.url;

        container.append(img);
        container.append(input);
        container.append(close);

        images.append(container);
      } else {
        return _orig_send_attachment.apply( this, [props, attachment] );
      };
    }
 
    wp.media.editor.open(button);
    return false;
  });
 
  $('.add_media').on('click', function(){
    _custom_media = false;
  });
});