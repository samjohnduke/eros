<?php

/**
 * Relative Includes
 * 
 * Load the files that are provided relative to the current path
 *
 */
function require_relative($array) {
  foreach ($array as $file) {
    if (!$filepath = locate_template($file)) {
      trigger_error(sprintf(__('Error locating %s for inclusion', 'eros'), $file), E_USER_ERROR);
    }
    require_once $filepath;
  }
  unset($file, $filepath);
}

/**
 * Eros includes
 *
 * The $eros_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 */
$eros_includes = array(
  'lib/utils.php',           // Utility functions
  'lib/init.php',            // Initial theme setup and constants
  'lib/wrapper.php',         // Theme wrapper class
  'lib/sidebar.php',         // Sidebar class
  'lib/config.php',          // Configuration
  'lib/activation.php',      // Theme activation
  'lib/titles.php',          // Page titles
  'lib/nav.php',             // Custom nav modifications
  'lib/gallery.php',         // Custom [gallery] modifications
  'lib/scripts.php',         // Scripts and stylesheets
  'lib/extras.php',          // Custom functions
  'lib/builder.php',         // Load the builder files

  'app/app_loader.php'       // Load the wordpress theme app files
);

//Load the core files
require_relative($eros_includes);

