<?php

class ThemeWidget extends ErosDashboardWidget {

  protected $theme_details;

  public function init() {
    $this->theme_details = wp_get_theme();
    $this->title = 'Eros Theme Specifics';
    $this->description = 'This theme is powered by the developers at Eros&rsquo; Wordpress &mdash; <i>wordpress with love</i>';
    $this->name = 'eros_theme_dash_widget';
    $this->template_path = locate_template('app/templates/dash_theme_widget.php');
  }

  public function version() {
    echo $this->theme_details->get('Version');
  }

  public function last_updated() {
    echo '3rd of March, 2015';
  }

  public function next_update() {
    echo '3rd of April, 2015';
  }

}