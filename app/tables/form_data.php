<?php

class FormDataTable extends ErosTable {

  //callback used to create the table in the database. 
  //sets up the sql used. Change this function if necessary
  //and the table will automagically update in the db.
  public function createTable($table) {
    $table->name('form_data');
    $table->increments('id');
    $table->integer('form_id');

    //Add the right form fields in here
    $table->string('title', 20);
    $table->string('first_name', 255);
    $table->string('last_name', 255);
    $table->string('email', 2048);
    $table->string('address', 1024);
    $table->string('occupation', 1024);
    $table->string('phone_number_one', 255);
    $table->string('state', 255);
    $table->string('referral', 1024);
    $table->text('message');

    $table->text('other', array('nullable' => false));

    $table->timeStamps();
    $table->softDeletes();
    $table->version('0.0.2');
  }

}
