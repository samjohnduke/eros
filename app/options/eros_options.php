<?php

$eros_settings_page = new ErosOptionsPage("options-general.php","eros");
$eros_settings_page
  ->page_title("Eros Wordpress Settings")
  ->menu_title("Eros")
  ->description("Setup the configuration settings for this website as required by the Eros Wordpress Theme")
  ->capability('manage_options')

  ->add_section("keys", function($section){
    $section
      ->title("Keys")
      ->description("Keys required for third party services")

      ->add_field("google_analytics", 'text', function($field) {
        $field->title('Google Analytics');
      })

      ->add_field("mailchimp", 'text', function($field) {
        $field->title('Mail Chimp');
      })

      ->add_field("mandrill_api_key", 'text', function($field) {
        $field->title('Mandrill Api Key');
      })

      ->add_field("mandrill_account_name", 'text', function($field) {
        $field->title('Mandrill Account Name');
      })


      ->add_field("eros_remote_id", 'text', function($field) {
        $field->title('Eros Remote Id');
      })

      ->add_field("eros_remote_secret", 'text', function($field) {
        $field->title('Eros Remote Secret');
      });
  })
  
  ->add_section('email', function($section){
    $section
      ->title("Email Notifications")
      ->description("The name and email address that new lead information will be forwarded to.")

      ->add_field("notification_email", 'text', function($field) {
        $field->title('Email Address');
      })

      ->add_field('notification_name', 'text', function($field) {
        $field->title('Name');
      });
  })
  
  ->add_section('profiles', function($section){
    $section
      ->title("User Profiles")
      ->description("Add sections to user profiles")

      ->add_field("user_images", 'check', function($field) {
        $field->title('Enable user images');
      })

      ->add_field('user_titles', 'check', function($field) {
        $field->title('Enable user title');
      });
  });