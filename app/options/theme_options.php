<?php

$eros_theme_page = new ErosOptionsPage("themes.php", "eros_theme_settings");
$eros_theme_page
  ->page_title("Eros Theme Settings")
  ->menu_title("Eros Theme")
  ->description("Configure the theme to include all the components you require")
  ->capability('edit_theme_options')

  ->add_section("general", function($section){
    $section
      ->title("General Theme Options")
      ->description("This section will enable you to setup the main components of you website.")

      ->add_field("wordpress_bar", 'check', function($field) {
        $field->title('Show wordpress bar');
      })

      ->add_field("top_bar_visible", 'check', function($field) {
        $field->title('Show top bar');
      })

      ->add_field("primary", 'color', function($field) {
        $field->title('Primary Color');
      })

      ->add_field("secondary", 'color', function($field) {
        $field->title('Secondary Color');
      })

      ->add_field("highlight", 'color', function($field) {
        $field->title('Highlight Color');
      })

      ->add_field("include_google_maps", 'check', function($field) {
        $field->title('Add Google Map');
      })

      ->add_field("include_testimonials", 'check', function($field) {
        $field->title('Show testimonials on home page');
      });

  })

  ->add_section('top_bar_section', function($section) {
    $section
      ->title('Top Bar')
      ->description('Change the settings of the topbar such as logo and position')

      ->add_field("affix-topbar-to-viewport", 'check', function($field) {
        $field->title('Fix top bar to top');
      })

      ->add_field("use_logo_for_topbar", 'check', function($field) {
        $field->title('Use logo in topbar');
      })

      ->add_field('logo_for_topbar', 'mediaupload', function($field) {
        $field->title('Add logo');
      });
  })

  ->add_section('header_section', function($section) {
    $section
      ->title('Header')
      ->description('This is the section for configuring the header. It includes options for colors, images and more.')

      ->add_field('header_type', 'dropdown', function($field) {
        $field->title('Header Type')
              ->configure(function($f){
                $f->selectOptions([
                  'color' => 'Flat Color',
                  'image' => 'Large Image',
                  'image_slider' => 'Image Slider',
                  'video' => 'Full Screen Video'
                ]);
              });
      })

      ->add_field('logo_for_header', 'mediaupload', function($field) {
        $field->title('Add logo');
      })

      ->add_field('images_for_header', 'mediamultiupload', function($field) {
        $field->title('Add images');
      })

      ->add_field('video_for_header', 'text', function($field) {
        $field->title('Add video url');
      })

      ->add_field("bk_color_for_header", 'color', function($field) {
        $field->title('Background Color');
      });

  })

  ->add_section('footer_section', function($section) {
    $section
      ->title('Footer')
      ->description('Configure the footer section as desired. Once you have made these changes you should
        go to the wordpress widgets section add content.')

      ->add_field('footer_type', 'dropdown', function($field) {
        $field->title('Footer Type')
              ->configure(function($f){
                $f->selectOptions([
                  'columns' => "Singular",
                  'separated' => 'Separated'
                ]);
              });
      })

      ->add_field('footer_column_number', 'dropdown', function($field) {
        $field->title('Footer Columns')
              ->configure(function($f){
                $f->selectOptions([
                  'one' => '1',
                  'two' => '2',
                  'three' => '3',
                  'four' => '4'
                ]);
              });
      });
  });