<?php
require_relative([
  'app/version.php',
  'app/models/forms.php',
  'app/options/eros_options.php',
  'app/options/theme_options.php',
  'app/tables/form.php',
  'app/dashboard.php',
  'app/widgets/dash/theme_widget.php'
]);

$dash = new ErosDashboard();
$theme_widget = new ThemeWidget();
$form_table = new FormTable();