<?php 

class ErosModel {

  public $id;

  //Set the table name for this model
  protected $table_name;

  //Enable or disable soft delete
  protected $soft_deleted;

  //Holds all the models attributes
  public $data = array();

  //is this a new record
  public $is_new; 

  //has the object been saved to the database since 
  //last change
  public $is_saved;
  public $is_dirty;

  //has the object been deleted
  public $is_deleted;

  public $errors;

  /**
   * Find
   *
   * Fetch from the database the model 
   * with an id provided.
   *
   * @return ErosModel|null the model
   */
  public static function find($id) {
    $query = "SELECT * FROM %s WHERE id={$id}";
    $results = ErosModel::fetch($query, get_called_class());
    return $results[0];
  }

  /**
   * Find By
   *
   * Fetch from the database an array of models which meets
   * the requirements of the arguments
   *
   * @return array[ErosModel] Array of model objects
   */
  public static function find_by($attr, $id) {
    return $this->where($attr, "=".$id);
  }

  /**
   * Create
   *
   * Create a new model from a set of params.
   * 
   * @param array Array of object params (unvalidated)
   * @return ErosModel the model being created
   */
  public static function create($params) {
    $ins = new static($params);
    $ins->is_new = true;
    $ins->created_at = time();
    return $ins;
  }

  /**
   * Where
   *
   * Get models that have some column that is comparable to the check
   * and the check must contain the comparison operator
   */
  public static function where($column, $check) {
    $model = __CLASS__;
    $sql = "SELECT * FROM %s WHERE {$column}{$check}";
    $results = $this->fetch('SELECT');
    return $results;
  }

  public static function all() {
    $query = "SELECT * FROM %s";
    $results = ErosModel::fetch($query, get_called_class());
    return $results;
  }

  /** 
   * NOT USED EXCEPT BY INTERAL FUNCTIONS
   */
  private static function fetch($query, $model) {
    global $wpdb;
    $dummy = new $model();
    $table_name = $wpdb->prefix.$dummy->table_name;
    $query = sprintf($query, $table_name);
    
    $rows = $wpdb->get_results($query, ARRAY_A);

    $output = array();
    foreach($rows as $row) {
      $m = new $model($row);
      $output[] = $m;
    }
    return $output;
  }

  private static function replace($table, $data) {
    global $wpdb;
    $table_name = $wpdb->prefix.$table;
    $wpdb->replace($table_name, $data);
    return $wpdb->insert_id;
  }

  private static function insert($table, $data) {
    global $wpdb;
    $table_name = $wpdb->prefix.$table;
    $wpdb->insert($table_name, $data);
    return $wpdb->insert_id;
  }

  private static function remove($table, $where) {
    global $wpdb;
    return $wpdb->delete($table, $where);
  }

  private static function table($table_name) {
    global $tables;
    return $tables->getTable($table_name);
  }

  /** 
   * Construct
   *
   * Create a new model instance from a number of params
   *
   * @param array Array of object params (unvalidated)
   */
  public function __construct($params = array()) {
    if(isset($params['other'])) {
      $other = unserialize($params['other']);
      unset($params['other']);

      foreach($other as $k => $v) {
        $params[$k] = $v;
      }
    }
    $this->is_dirty = false;
    $this->data = $params;
  }

  /**
   * Save
   * 
   * Save all the params of this model into the database.
   * 
   * @return boolean Success of the function
   */
  public function save() {
    $data = $this->data;
    $table = ErosModel::table($this->table_name);
    $other = array();

    foreach ($data as $key => $value) {
      if(!$table->hasColumn($key)) {
        $other[$key] = $value;
        unset($data[$key]);
      }
    }

    $data['other'] = serialize($other);

    if($this->is_new) {
      $result = ErosModel::insert($this->table_name, $data);
    } else {
      $result = ErosModel::replace($this->table_name, $data);
    }

    if($result) {
      $this->id = $result;
      $this->is_saved = true;
    }

    return $result;
  }

  /**
   * Update
   *
   * Change the current object state of the world
   *
   * @params array Array of params to update
   */
  public function update($params = array()) {
    $this->updateParams($params);
    $this->save();
  }

  /**
   * Delete
   *
   * Remove the model from the database
   *
   * @params array Array of params to update
   * @return boolean Success of the function
   */
  public function delete() {
    return ErosModel::remove($this->table_name, array('id' => $this->id));
  }

  /**
   * 
   */
  public function listen($event, $clsure) {

  }

  /**
   * Get the value of a data property within the 
   * models values other return null
   */
  public function __get($name) {
    if (array_key_exists($name, $this->data)) {
      return $this->data[$name];
    }

    return null;
  }

  /**
   * Set the value of data property within the model
   */
  public function __set($name, $value) {
    $this->data[$name] = $value;
    $this->is_dirty = true;
  }

  /**
   * Ask if the models property exists
   */
  public function __isset($name) {
    return isset($this->data[$name]);
  }

  /**
   * Remove the value and name from the data properties
   */
  public function __unset($name) {
    unset($this->data[$name]);
  }


  public function valid($form_type) {
    $method = 'validation_'.$form_type;
    $rules = $this->$method();
    $errors = array();

    foreach($rules as $k => $rule) {
      if($rule['type'] == 'email') {
        $email = sanitize_email($this->data[$k]);

        if(!is_email($email)) {
          $errors[$k] = 'Email field is not an valid email address';
        }
        $this->data[$k] = $email;
      } else {
        $this->data[$k] = sanitize_text_field($this->data[$k]);
      }

      if($rule['required']) {
        if(!isset($this->data[$k]) || strlen($this->data[$k]) == 0) {
          $errors[$k] = $rule['message'];
        }
      }
    }

    $this->errors = $errors;

    if(count($errors) == 0) {
      return true;
    } else {
      return false;
    }

  }

  private function updateParams($params) {
    $this->data = array_merge($this->data, $params);
  }

  /**
   * run input against validations
   */
  private function validate($input) {
    $output = array();

    foreach($input as $k => $v) {
      $output[$k] = $v;
    }

    return $output;
  }

  /**
   * Array containing fields which will be used to validate against
   */
  protected function validateFields() {

  }

}