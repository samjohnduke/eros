<?php 

class ErosTypeBuilder {
  private $type_name;
  private $singular;
  private $plural;
  private $slug;

  private $is_public;
  private $exclude_from_search;
  private $publicly_queryable;
  private $show_ui;
  private $show_in_nav_menus;
  private $show_in_menu;
  private $show_in_admin_bar;

  private $menu_position;
  private $menu_icon;

  private $capability_type;
  private $capabilities;
  private $map_meta_cap;

  private $hierarchical;
  private $supports;

  private $taxonomies;
  private $register_meta_box_cb;
  private $has_archive;
  private $rewrite;

  private $query_var;
  private $can_export;

  private $text_domain;
  public $meta_boxes;

  /**
   * Set up defaults to build the custom post type
   */
  public function __construct($name) {
    $this->type_name = $name;
    $this->public = false;
    $this->menu_position = 30;
    $this->capability_type = 'post';
    $this->capabilities = array();
    $this->map_meta_cap = true;
    $this->hierarchical = false;
    $this->supports = array("title", 'editor', 'thumbnail');
    $this->taxonomies = array();
    $this->has_archive = $name;
    $this->query_var = false;
    $this->can_export = true;
    $this->text_domain = 'eros';
    $this->meta_boxes = array();

    $that = $this;

    $this->register_meta_box_cb = function() use($that) {
      foreach($that->meta_boxes as $mb) { $mb->build(); }
    };
  }

  /**
   * To Args
   *
   * Converts the builder object into a set of arguments that can be added
   * used to build the post type in wordpress
   *  
   * @return array The arguments used to build custom post type
   */
  public function toArgs() {
    return $this->buildArgs();
  }

  /**
   * Build Labels
   *
   * Take the names used within the custom post type and create 
   * an array of labels needed by wordpress
   *
   * @return array The array of labels for cusomt post type
   */
  private function buildLabels() {
    $singular = $this->toSingular();
    $plural = $this->toPlural();

    $labels = array(
      'name'               => sprintf( __( '%s', $this->text_domain ), $plural ),
      'singular_name'      => sprintf( __( '%s', $this->text_domain ), $singular ),
      'menu_name'          => sprintf( __( '%s', $this->text_domain ), $plural ),
      'all_items'          => sprintf( __( '%s', $this->text_domain ), $plural ),
      'add_new'            => __( 'Add New', $this->text_domain ),
      'add_new_item'       => sprintf( __( 'Add New %s', $this->text_domain ), $singular ),
      'edit_item'          => sprintf( __( 'Edit %s', $this->text_domain ), $singular ),
      'new_item'           => sprintf( __( 'New %s', $this->text_domain ), $singular ),
      'view_item'          => sprintf( __( 'View %s', $this->text_domain ), $singular ),
      'search_items'       => sprintf( __( 'Search %s', $this->text_domain ), $plural ),
      'not_found'          => sprintf( __( 'No %s found', $this->text_domain ), $plural ),
      'not_found_in_trash' => sprintf( __( 'No %s found in Trash', $this->text_domain ), $plural ),
      'parent_item_colon'  => sprintf( __( 'Parent %s:', $this->text_domain ), $singular )
    );

    return $labels;
  }

  /**
   * Build Args
   *
   * Create an array from internal object structure
   * 
   * @param array The args used to build a custom post type
   */
  private function buildArgs() {
    $labels = $this->buildLabels();

    $args = array(
      'labels'             => $labels,
      'public'             => $this->is_public,
      'publicly_queryable' => $this->publicly_queryable,
      'exclude_from_search' => $this->exclude_from_search,
      'show_ui'            => $this->show_ui,
      'show_in_menu'       => $this->show_in_menu,
      'query_var'          => $this->query_var,
      'rewrite'            => $this->rewrite,
      'capability_type'    => $this->capability_type,
      'has_archive'        => $this->has_archive,
      'hierarchical'       => $this->hierarchical,
      'menu_position'      => $this->menu_position,
      'menu_icon'          => $this->menu_icon,
      'supports'           => $this->supports,
      'map_meta_cap'       => $this->map_meta_cap,
      'register_meta_box_cb' => $this->register_meta_box_cb
    );

    return $args;
  }

  /**
   * To Human
   *
   * Make a name, specifically the post name, suitable for humans
   *
   * @param string $name The name you want to make friendly.
   * @return string The human friendly name.
   */
  private function toHuman($name) {
    return ucwords( strtolower( str_replace( "-", " ", str_replace( "_", " ", $name ) ) ) );
  }

  /**
   * To Singular
   *
   * Make the post type name singular ONLY if the user has not previsouly 
   * set the name themselves
   *
   * @return string The singular version of the name
   */
  private function toSingular() {
    if($this->singular) { return $this->singular; }
    return $this->toHuman($this->type_name);
  }

  /**
   * To Plural
   *
   * Make the post type name plural ONLY if the user has not previsouly 
   * set the name themselves
   *
   * @return string The plural version of the name
   */
  private function toPlural() {
    if($this->plural) { return $this->plural; }
    return $this->toHuman($this->type_name) . "s";
  }


  /**
   * Singular
   *
   * Set the singular name of the post type
   * 
   * @param string The singular name
   * @param ErosTypeBuild self for method chaining
   */
  public function singular($str) {
    $this->singular = $str;
    return $this;
  }

  /**
   * Plural
   *
   * Set the plural name of the post type
   * 
   * @param string The plural name
   * @param ErosTypeBuilder self for method chaining
   */
  public function plural($str) {
    $this->plural = $str;
    return $this;
  }

  public function slug($str) {
    $this->slug = $str;
    return $this;
  }

  public function isPublic($bool) {
    $this->is_public = $bool;
    $this->exclude_from_search = $bool;
    $this->show_ui = $bool;
    $this->publicly_queryable = $bool;
    $this->show_in_nav_menus = $bool;
    $this->show_in_menu = $bool;
    $this->show_in_admin_bar = $bool;
    return $this;
  }

  public function excludeFromSearch($bool) {
    $this->exclude_from_search = $bool;
    return $this;
  }

  public function publiclyQueryable($bool) {
    $this->publicly_queryable = $bool;
    return $this;
  }

  public function showUi($bool) {
    $this->show_ui = $bool;
    return $this;
  }

  public function showInNavMenus($bool) {
    $this->show_in_nav_menus = $bool;
    return $this;
  }

  public function showInMenu($bool) {
    $this->show_in_menu = $bool;
    return $this;
  }

  public function showInAdminBar($bool) {
    $this->show_in_admin_bar = $bool;
    return $this;
  }

  public function menuPosition($int) {
    $this->menu_position = $int;
    return $this;
  }

  public function menuIcon($str) {
    $this->menu_icon = $str;
    return $this;
  }

  public function capabilityType($str) {
    $this->capability_type = $str;
    return $this;
  }

  public function capabilities($array) {
    $this->capabilities = $array;
    return $this;
  }

  public function mapMetaCap($bool) {
    $this->singular = $str;
    return $this;
  }

  public function hierarchical($bool) {
    $this->hierarchical = $bool;
    return $this;
  }

  public function supports($array) {
    $this->supports = $str;
    return $this;
  }

  public function addSupport($option) {
    if(!in_array($option, $this->supports)) {
      $this->supports[] = $option;
    }
    return $this;
  }

  public function removeSupport($option) {
    if(in_array($option, $this->supports)) {
      $key = array_search($option, $this->supports);
      unset($this->supports[$key],$key);
    }
    return $this;
  }

  public function registerMetaBoxCb($closure) {
    $this->register_meta_box_cb = $closure;
    return $this;
  }

  public function hasArchive($boolOrStr) {
    $this->has_archive = $boolOrStr;
    return $this;
  }

  public function rewrite($boolOrArray) {
    $this->rewrite = $boolOrArray;
    return $this;
  }

  public function queryVar($boolOrStr) {
    $this->query_var = $boolOrStr;
    return $this;
  }

  public function canExport($bool) {
    $this->can_export = $bool;
    return $this;
  } 

  public function textDomain($str) {
    $this->text_domain = $str;
    return $this;
  }

  public function addMetaBox($name, $closure) {
    $meta_box = new ErosMetaBox($name, $this->type_name);
    $closure($meta_box);
    $this->meta_boxes[] = $meta_box;
    return $this;
  }

  public function addTaxonomy($str) {
    $this->taxonomies[] = $str;
    return $this;
  }

  public function addNewTaxonomy($name, $closure) {
    $taxon = new ErosTaxonomy($name);
    $closure($taxon);
    $this->addTaxonomy($name);
    return $this;
  } 

  /**
   * Belongs To
   *
   * Setup a relationship between this post type and another such that
   * this one belongs to the other. This method only sets up the relationship
   * and assumes that the other post type exists
   *  
   * @param string The name of the post type this belongs to
   * @param function Closure used to configure relationship
   * @returns CustomPostType Self for method chaining.
   */
  public function belongs_to($post_type, $closure) {
    return $this;
  }

  /**
   * Has Many
   *
   * Setup a relationship between this post type and another such that
   * this one has many of the other. This method only sets up the relationship
   * and assumes that the other post type exists
   *  
   * @param string The name of the post type this has many of
   * @returns CustomPostType Self for method chaining.
   */
  public function has_many($post_type) {
    return $this;
  }

  /**
   * Has Many Through
   *
   * Setup a relationship between this post type and another such that
   * this one has many to the other through an intermediate object. 
   * This method is used to create many to many relationships and can store
   * custom relationship information
   *
   * This method only sets up the relationship and assumes that the other 
   * post type exists
   *  
   * @param string The name of the post type this one has many of.
   * @param string The name of the post type through which the relationship is established
   * @returns CustomPostType Self for method chaining.
   */
  public function has_many_through($post_type_many, $post_type_through) {
    return $this;
  }
}