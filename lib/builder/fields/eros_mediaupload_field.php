<?php

class ErosMediauploadField {

  /**
   * Used for reference the fields value from wordpress
   */
  public $field_name;

  public function __construct($field_name) {
    $this->field_name = $field_name;
  }

  //Validate this fields input.
  //Temporaily just return it.
  public function sanitize($input) {
    return $input;
  }

  //Render out the form needed to display this field
  public function render($args) {
    $field = $this->field_name;
    $value = get_option($field);

    echo '<div class="uploader">';
    echo sprintf('<input id="%s" name="%s" type="text" value="%s" />', $field, $field, $value);
    echo sprintf('<input id="%s_button" class="button" name="%s_button" type="button" value="Select" />', $field, $field);
    echo '</div>';
  }
}