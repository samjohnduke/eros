<?php

class ErosTextField {

  /**
   * Used for reference the fields value from wordpress
   */
  public $field_name;

  public function __construct($field_name) {
    $this->field_name = $field_name;
  }

  //Validate this fields input.
  //Temporaily just return it.
  public function sanitize($input) {
    return $input;
  }

  //Render out the form needed to display this field
  public function render($args) {
    $field = $this->field_name;
    // Get the value of this setting
    $value = get_option($field);
    echo sprintf('<input class="regular-text" type="text" name="%s" id="%s" value="%s" />', $field, $field, $value);
  }
}