<?php

class ErosMediamultiuploadField {

  /**
   * Used for reference the fields value from wordpress
   */
  public $field_name;

  public function __construct($field_name) {
    $this->field_name = $field_name;
  }

  //Validate this fields input.
  //Temporaily just return it.
  public function sanitize($input) {
    return $input;
  }

  //Render out the form needed to display this field
  public function render($args) {
    $field = $this->field_name;
    $value = get_option($field);

    if(!$value) {
      $value = array();
    }

    echo '<div class="multi-uploader">';
    echo sprintf('<input id="%s_button" class="button" name="%s_button" type="button" value="Add" />', $field, $field);
    echo sprintf('<ul id="%s_images" class="images">', $field); 

    foreach($value as $img) {
      echo sprintf('<li class="media"><input name="%s[]" type="hidden" value="%s" />', $field, $img);
      $url = wp_get_attachment_thumb_url( $img );
      echo sprintf("<img id='%s_%s_input' src='%s' /><button>x</button></li>", $field, $count, $url);
    }

    echo '</ul></div>';   
  }
}