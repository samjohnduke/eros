<?php

class ErosCheckField {

  /**
   * Used for reference the fields value from wordpress
   */
  public $field_name;

  public function __construct($field_name) {
    $this->field_name = $field_name;
  }

  //Validate this fields input.
  //Temporaily just return it.
  public function sanitize($input) {
    var_dump($input);
    return $input;
  }

  //Render out the form needed to display this field
  public function render($args) {
    $field = $this->field_name;
    $value = get_option($field);

    $checked = 'checked';
    if($value != 'yes') {
      $checked = '';
    }

    echo '<label><input type="checkbox" '.$checked.' class="' . esc_attr($field) . '" name="' . esc_attr($field) . '" value="yes" /></label>';
  }
}