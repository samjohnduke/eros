<?php 

class ErosDropdownField {

  /**
   * Used for reference the fields value from wordpress
   */
  private $field_name;

  /**
   *
   */
  private $options;

  /**
   *
   */
  public function __construct($field_name) {
    $this->field_name = $field_name;
  }

  //Validate this fields input.
  //Temporaily just return it.
  public function sanitize($input) {
    return $input;
  }

  //Render out the form needed to display this field
  public function render($args) {
    $field = $this->field_name;
    // Get the value of this setting
    $value = get_option($field);

    echo sprintf('<select name="%s" class="regular-text">', $this->field_name);

    foreach($this->options as $k => $v) {
      if($k == $value) { $s = 'selected="selected"'; } else { $s = ''; }
      echo sprintf("<option %s value='%s'>%s</option>", $s, $k, $v);

    }

    echo '</select>';
  }

  public function configure($closure) {
    $closure($this);
  }

  public function selectOptions($options) {
    $this->options = $options;
  }
}