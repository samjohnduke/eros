<?php wp_nonce_field( 'eros_meta_box_'.$this->id, 'eros_nonce_' . $this->id ); ?>
<div>
  <label for="">
    <p><?php echo $this->description; ?></p>
  </label>
  <input class="widefat" type="text" name="<?php echo $this->id; ?>" id="<?php echo $this->id; ?>" value="<?php echo esc_attr( get_post_meta( $object->ID, $this->id, true ) ); ?>" />
</div>