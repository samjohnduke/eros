<div class='<?php echo $this->page; ?>'>
  <div class='wrap'>
    <h2><?php echo $this->page_options['page_title']; ?></h2>
    <form name="form" action="options.php" method="post">
      <p><?php $this->page_options['description']?></p>
      <?php settings_fields($this->page); ?>
      <?php do_settings_sections($this->page); ?>
      <?php submit_button(); ?>
    </form>
  </div>
</div>