<?php

/**
 * This class is a simple remote resource base
 * for working with remote resources such as blog posts
 * or anything. 
 */
class ErosActiveResource {

  protected $resource_name;
  protected $base_url;
  protected $secure;
  protected $api_id;
  protected $api_secret;
  protected $port;
  protected $headers;

  private $data;

  public function __construct($params = array()) {

  }

  public static function create($params = array()) {

  }

  public static function find($id) {

  }

  public static function all() {

  }

  public static function find_by($attr, $value) {

  }

  public function update($params = array()) {

  }

  public function save() {
    
  }

}