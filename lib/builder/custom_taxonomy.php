<?php

class CustomTaxonomy {
  private $name;
  private $post_type;
  private $singluar;
  private $plural;

  private $is_public;
  private $show_ui;
  private $show_in_nav_menus;
  private $show_tagcloud;
  private $meta_box_cb;
  private $show_admin_column;

  private $hierarchical;
  private $query_var;

  private $rewrite;
  private $capabilities;
  private $sort;

  /**
   * Initialize the taxon's vars to set it to be a private taxonomy
   */
  public function __construct($name) {
    $this->post_type = null;
    $this->name = $name;
    $this->is_public = false;
    $this->hierarchical = false;
    $this->rewrite = false;

    if(!$this->exists()) {
      add_action('init', array($this, 'build'));
    }
  }

  //Create the actual taxonomy
  public function build() {
    if($this->exists()) { return false; }

  }

  public function post_type($type) {
    $this->post_type = $type;
    return $this;
  }

  public function singular($name) {
    $this->singular = $name;
    return $this;
  }

  public function plural($name) {
    $this->singular = $name;
    return $this;
  }

  public function is_public($bool) {
    $this->is_public = $bool;
    $this->show_ui = $bool;
    $this->show_in_nav_menus = $bool;
    $this->show_tagcloud = $bool;
    return $this;
  }

  public function show_ui($bool) {
    $this->show_ui = $bool;
    return $this;
  }

  public function show_in_nav_menus($bool) {
    $this->show_in_nav_menus = $bool;
    return $this;
  }

  public function show_tagcloud($bool) {
    $this->show_tagcloud = $bool;
    return $this;
  }

  public function meta_box_cb($closure) {
    $this->meta_box_cb = $closure;
    return $this;
  }

  public function show_admin_column($bool) {
    $this->show_admin_column = $bool;
    return $this;
  }

  public function hierarchical($bool) {
    $this->hierarchical = $bool;
    return $this;
  }

  public function query_var($strOrBool) {
    $this->query_var = $strOrBool;
    return $this;
  }

  public function rewrite($str) {
    $this->rewrite = $str;
    return $this;
  }

  public function capabilities($array) {
    $this->capabilities = $array;
    return $this;
  }

  public function sort($bool) {
    $this->sort = $bool;
    return $this;
  }

  public function exists() {
    return taxonomy_exists($this->name);
  }
}