<?php 

abstract class ErosDashboardWidget {
    
  protected $name;
  protected $title;
  protected $description;
  protected $template_path;

  public function __construct() {
    $this->init();
    add_action('wp_dashboard_setup', array(&$this, 'build'));
  }

  public function render() {
    include($this->template_path);
  }

  public function build() {
    wp_add_dashboard_widget(
      $this->name,         // Widget slug.
      $this->title,         // Title.
      array(&$this, 'render') // Display function.
    );  
  }

  abstract public function init();
}