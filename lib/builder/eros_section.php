<?php

/**
 * This class encapsulates settings sections
 * 
 * @since 0.0.1
 * @author Sam John Duke
 */
class ErosSection {

  /**
   * The name of the settings section
   */
  private $name;

  /**
   * Text to be diplay right after the settings description
   */
  private $description;

  /**
   * 
   */
  private $fields;

  /**
   *
   */
  public function __construct($name) {
    $this->name = $name;
  }

  /**
   * Set the title of this section
   */
  public function title($title) {
    $this->title = $title;
    return $this;
  }

  /**
   * Set the description of this section
   */
  public function description($description) {
    $this->description = $description;
    return $this;
  }

  /**
   *
   */
  public function add_field($field_name, $type, $closure = null) {
    $field = new ErosField($field_name, $type);
    if($closure) { $closure($field); }
    $this->fields[] = $field;
    return $this;
  }

  /**
   *
   */
  public function add_to_page($page_name) {
    //Create the section in wordpress
    add_settings_section(
      $this->name,     // section id
      $this->title,    // section title
      array(&$this, 'render'),              // add preamble text
      $page_name       // page name
    );

    foreach($this->fields as $section) {
      $section->add_to_section($page_name, $this->name);
    }
  }

  public function render() {
    echo "<p>".$this->description."</p>";
  }
}