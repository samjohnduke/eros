<?php

class ErosTaxonomy {
  private $name;
  private $post_type;
  private $singular;
  private $plural;

  private $is_public;
  private $show_ui;
  private $show_in_nav_menus;
  private $show_tagcloud;
  private $meta_box_cb;
  private $show_admin_column;

  private $hierarchical;
  private $query_var;

  private $rewrite;
  private $capabilities;
  private $sort;

  /**
   * Initialize the taxon's vars to set it to be a private taxonomy
   */
  public function __construct($name) {
    $this->post_type = null;
    $this->name = $name;
    $this->is_public = false;
    $this->hierarchical = false;
    $this->rewrite = false;
    $this->capabilities = array('manage_terms', 'edit_terms', 'delete_terms', 'assign_terms');
    $this->show_admin_column = true;
    $this->meta_box_cb = null;

    if(!$this->exists()) {
      add_action('init', array($this, 'build'));
    }
  }

  //Create the actual taxonomy
  public function build() {
    if($this->exists()) { return false; }

    register_taxonomy($this->name, $this->post_type, $this->buildArgs());
  }

  public function post_type($type) {
    $this->post_type = $type;
    return $this;
  }

  public function singular($name) {
    $this->singular = $name;
    return $this;
  }

  public function plural($name) {
    $this->plural = $name;
    return $this;
  }

  public function isPublic($bool) {
    $this->is_public = $bool;
    $this->show_ui = $bool;
    $this->show_in_nav_menus = $bool;
    $this->show_tagcloud = $bool;
    return $this;
  }

  public function show_ui($bool) {
    $this->show_ui = $bool;
    return $this;
  }

  public function show_in_nav_menus($bool) {
    $this->show_in_nav_menus = $bool;
    return $this;
  }

  public function show_tagcloud($bool) {
    $this->show_tagcloud = $bool;
    return $this;
  }

  public function meta_box_cb($closure) {
    $this->meta_box_cb = $closure;
    return $this;
  }

  public function show_admin_column($bool) {
    $this->show_admin_column = $bool;
    return $this;
  }

  public function hierarchical($bool) {
    $this->hierarchical = $bool;
    return $this;
  }

  public function query_var($strOrBool) {
    $this->query_var = $strOrBool;
    return $this;
  }

  public function rewrite($str) {
    $this->rewrite = $str;
    return $this;
  }

  public function capabilities($array) {
    $this->capabilities = $array;
    return $this;
  }

  public function sort($bool) {
    $this->sort = $bool;
    return $this;
  }

  public function exists() {
    return taxonomy_exists($this->name);
  }

  private function buildLabels() {
    $singular = $this->singular;
    $plural = $this->plural;

    $labels = array(
      'name'               => sprintf( __( '%s', 'eros' ), $plural ),
      'singular_name'      => sprintf( __( '%s', 'eros' ), $singular ),
      'menu_name'          => sprintf( __( '%s', 'eros' ), $plural ),
      'all_items'          => sprintf( __( '%s', 'eros' ), $plural ),
      'add_new_item'       => sprintf( __( 'Add New %s', 'eros' ), $singular ),
      'edit_item'          => sprintf( __( 'Edit %s', 'eros' ), $singular ),
      'update_item'          => sprintf( __( 'Edit %s', 'eros' ), $singular ),
      'new_item'           => sprintf( __( 'New %s', 'eros' ), $singular ),
      'view_item'          => sprintf( __( 'View %s', 'eros' ), $singular ),
      'search_items'       => sprintf( __( 'Search %s', 'eros' ), $plural ),
      'not_found'          => sprintf( __( 'No %s found', 'eros' ), $plural ),
      'not_found_in_trash' => sprintf( __( 'No %s found in Trash', 'eros' ), $plural ),
      'parent_item_colon'  => sprintf( __( 'Parent %s:', 'eros' ), $singular )
    );

    return $labels;
  }

  private function buildArgs() {
    $labels = $this->buildLabels();

    $args = array(
      'labels'             => $labels,
      'public'             => $this->is_public,
      'show_ui'            => $this->show_ui,
      'show_in_nav_menus'  => $this->show_in_nav_menus,
      'show_tagcloud'      => $this->show_tagcloud,
      'show_admin_column'  => $this->show_admin_column,
      'query_var'          => $this->query_var,
      'rewrite'            => $this->rewrite,
      'capabilities'       => $this->capabilities,
      'hierarchical'       => $this->hierarchical
    );

    return $args;
  }

}