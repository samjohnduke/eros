<?php

/**
 * Create a new settings field. 
 * 
 * @since 0.0.1
 * @author Sam John Duke
 */
class ErosField {

  /**
   * The name of the settings option
   */
  private $name;

  /**
   * What type of field is it. see the fields folder for possible 
   * type options.
   */
  private $type;

  /**
   * Fields are generic and named objects that 
   * can be instantied. This means that that they can be reused as they
   * are not isolated to settings. (can be used in meta boxes for example)
   */
  private $field;

  /**
   * Short text description to go below the field to let the user know 
   * something specific when entering option
   */
  private $description;

  /**
   * Set inital settings and create the internal field
   */
  public function __construct($name, $type) {
    $this->name = $name;
    $this->type = $this->classify($type);
    $this->field = $this->create_field();
  }

  /**
   * Some fields need to be configured. If that is the case 
   * you can pass a closure directly to the fields configure method
   */
  public function configure($closure) {
    $this->field->configure($closure);
    return $this;
  }

  /**
   *
   */
  public function add_to_section($page_name, $section_name) {
    //Create the section in wordpress
    add_settings_field(
      $this->name,                      // section id
      $this->title,                     // section title
      array(&$this->field, 'render'),   // callback (empty by default)
      $page_name,                       // page name
      $section_name,                    // section name
      array(
        
      )
    );

    //Create the setting in the database
    register_setting(
      $page_name, 
      $this->name, 
      array(&$this->field, 'sanitize')
    );
  }

  /**
   * Title
   *
   * Set the title for this field
   * 
   * @param string The title string to be displayed
   * @return ErosField self for method chaining
   */
  public function title($title) {
    $this->title = $title;
    return $this;
  }

  /**
   * Description
   *
   * Set the description for this field
   * 
   * @param string The description string to be displayed
   * @return ErosField self for method chaining
   */
  public function description($str) {
    $this->description = $str;
    return $this;
  }

  /**
   * This will take the internal representation of the field type and return 
   * an instantiated field object
   */
  private function create_field() {
    $classname = "Eros".$this->type."Field";
    return new $classname($this->name);
  }

  /**
   * This will take the type and turn is into a string that could represent
   * a classname
   */
  private function classify($type) {
    $lower = strtolower($type);
    $cap = ucfirst($lower);
    return $cap;
  }
}