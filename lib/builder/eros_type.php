<?php

/**
 * Eros Type
 *
 * A simple post type creator, used for creating a new Post type
 */
abstract class ErosType {

  /**
   * The type name is the internal structure used by wordpress
   * to store and reference information related to a post
   */
  protected $type_name;

  /**
   * A type builder is used to design the custom post type in wordpress.
   * is stores and transforms data as required.
   */
  protected $type_builder;

  /**
   * A relationship builder is used to create the relationships between 
   * objects in wordpress. It uses a simple interface to build into the core
   * of the system a mapping. The mapping can then be query to fetch data.
   */
  protected $relationship_builder;

  /**
   * A page builder is used to design the admin screen wordpress.
   */
  protected $page_builder;

  /** 
   *
   */
  public function __construct() {
    $this->type_name = get_called_class();
    $this->type_builder = new ErosTypeBuilder($this->type_name);
    $this->relationship_builder = new ErosRelationshipBuilder($this->type_name);
    $this->page_builder = new ErosPageBuilder($this->type_name);
    $this->setup();

    if(!$this->exists()) {
      add_action('init', array($this, 'build'));
    }
  }

  public function setup() {
    $this->init($this->type_builder);
    $this->fields($this->type_builder);
    $this->relationships($this->relationship_builder);
    $this->render($this->page_builder);
  }

  /**
   * Build
   * 
   * Take the custom post type and inject it into wordpress
   */
  public function build() {
    register_post_type($this->type_name, $this->type_builder->toArgs());
  }

  abstract protected function init($builder);
  abstract protected function relationships($builder);
  abstract protected function fields($builder);
  abstract protected function render($builder); 
  

  /**
   * Exists
   *
   * Perform a test to see if this custom post type already exists.
   *
   * @return bool The existence or not of the custom post type
   */
  private function exists() {
    return post_type_exists($this->post_type_name);
  }
}