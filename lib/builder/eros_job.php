<?php

abstract class ErosJob {

  protected $recurring = true;
  protected $recurs = 'hourly';
  protected $timestamp;
  protected $name;

  abstract public function perform();

  public function __construct($subname, $args) {
    $this->parse_args($args);
    $this->name = $subname.'_'.$this->name;

    if(!$this->timestamp) {
      $this->timestamp = time();
    }

    add_action( 'wp', array($this, 'schedule') );
    add_action( $this->name, array($this, 'perform') );
  }

  public function schedule() {
    if( !wp_next_scheduled( $this->name ) ) {
      if(!$this->recurring) {
        wp_schedule_single_event( $this->timestamp, $this->name );
      } else {
        wp_schedule_event( $this->timestamp, $this->recurs, $this->name);
      }
    }
  }


  private function parse_args($args) {
    if(isset($args['when'])) {
      $this->timestamp = $args['when'];
    }

    if(isset($args['recurring'])) {
      $this->recurring = $args['recurring'];
    }

    if(isset($args['recurs'])) {
      $this->recurring = $args['recurs'];
    }

  }
}