<?php 

abstract class ErosTable {

  private $table_name;
  private $columns;
  private $keys;
  private $sql;
  private $charset;
  private $version;
  private $column_names;

  abstract public function createTable($table);

  public function __construct() {  
    $this->columns = array();
    $this->column_names = array();
    $this->keys = array('primary' => array(), 'foreign' => array(), 'unique' => array(), "other" => array() );
    $this->sql = '';
    $this->createTable($this);
    add_action('after_setup_theme', array($this,'install'));
  }

  /**
   * Install
   *
   * Take a sql string and install or update the database. Is executed
   * after all the plugins have loaded.
   */
  public function install() {
    if($this->requiresUpdate()) {
      global $wpdb;
      $this->sql = $this->buildTable($wpdb->prefix, $wpdb->get_charset_collate());
      require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
      dbDelta( $this->sql );
      $this->updateVersion();
    }
  }

  /**
   * Build table
   * 
   * Turn the set of columns and keys into a string of sql used to 
   * build the table in the database
   *
   * @param string The database table prefix provided by wordpress
   * @param charset Charset of the database in wordpress
   * @return ErosTable self for method chaining
   */
  public function buildTable($prefix, $charset) {
    $table_name = $prefix.$this->table_name;

    $sql = '';
    $sql .= "CREATE TABLE ".$table_name." (\n";

    foreach($this->columns as $column) {
      $sql .=  rtrim(preg_replace('/\s+/', ' ',$column), ' ') . ",\n";
    }
      
    foreach($this->keys['primary'] as $key) {
      $sql .= rtrim(preg_replace('/\s+/', ' ',$key), ' ') . ",\n";
    }

    foreach($this->keys['foreign'] as $key) {
      $sql .= rtrim(preg_replace('/\s+/', ' ',$key), ' ') . ",\n";
    }

    foreach($this->keys['unique'] as $key) {
      $sql .= rtrim(preg_replace('/\s+/', ' ',$key), ' ') . ",\n";
    }

    foreach($this->keys['other'] as $key) {
      $sql .= rtrim(preg_replace('/\s+/', ' ',$key), ' ') . ",\n";
    }

    $sql = rtrim($sql, ",\n");

    $sql .= ') '.$charset.';';

    return $sql;
  }

  /**
   * Name
   * 
   * Set the tables name
   */
  public function name($name) {
    $this->table_name = $name;
    return $this;
  }
  /**
   * Big Increments
   * 
   * Add a big increments column with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function bigIncrements($name, $options=array()) {
    $col = "$name bigint UNSIGNED NOT NULL AUTO_INCREMENT";
    $this->columns[] = $col;
    $this->column_names[] = $name;

    $this->primary($name);
    return $this;
  }

  /**
   * Big Integer
   * 
   * Add a big integer column with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function bigInteger($name, $options=array()) {
    $type = 'bigint';
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Binary
   * 
   * Add a binary column with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function binary($name, $options=array()) {
    $type = 'binary';
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }
  
  /**
   * Boolean
   * 
   * Add a datetime column with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function boolean($name, $options=array()) {
    $type = 'bit';
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Char
   * 
   * Add a char column with the provided name
   *
   * @param string The column name
   * @param integer the length of the char
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function char($name, $length, $options=array()) {
    $type = "char($length)";
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Date 
   * 
   * Add a date column with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function date($name, $options=array()) {
    $type = 'date';
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Date Time
   * 
   * Add a datetime column with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function dateTime($name, $options=array()) {
    $type = 'datetime';
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Decimal
   * 
   * Add a decimal column with the provided name
   *
   * @param string The column name
   * @param integer The precision of the double
   * @param integer The scale of the double
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function decimal($name, $precision, $scale, $options=array()) {
    $type = "decimal($precision, $scale)";
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Double
   * 
   * Add a double column with the provided name
   *
   * @param string The column name
   * @param integer The precision of the double
   * @param integer The scale of the double
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function double($name, $precision, $scale, $options=array()) {
    $type = "double($precision, $scale)";
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Enum
   * 
   * Add a enum column with the provided name
   *
   * @param string The column name
   * @param array List of possible values this enum can hold
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function enum($name, $choices=array(), $options=array()) {
    $choices = join(',', $choices);
    $type = "enum($choices)";
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Float
   * 
   * Add a float column with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function float($name, $options=array()) {
    $type = "float($precision, $scale)";
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Increments
   * 
   * Add a increments column with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function increments($name, $options=array()) {
    $col = "$name bigint UNSIGNED NOT NULL AUTO_INCREMENT";
    $this->columns[] = $col;
    $this->column_names[] = $name;
    $this->primary($name);
    return $this;
  }

  /**
   * Integer
   * 
   * Add a integer column with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function integer($name, $options=array()) {
    $type = "int";
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Long Text
   * 
   * Add a long text column with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function longText($name, $options=array()) {
    $type = "longtext";
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Medium Integer
   * 
   * Add a medium integer column with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function mediumInteger($name, $options=array()) {
    $type = "mediumint";
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Medium Text
   * 
   * Add a time column with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function mediumText($name, $options=array()) {
    $type = "mediumtext";
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Small Integer
   * 
   * Add a small integer column with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function smallInteger($name, $options=array()) {
    $type = "smallint";
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Tiny Integer
   * 
   * Add a tiny integer column with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function tinyInteger($name, $options=array()) {
    $type = "tinyint";
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Soft Deletes
   * 
   * Add a timestamp column with the with the name deleted_at
   * and allows for soft deletes in the backed model
   *
   * @return ErosTable self for method chaining
   */
  public function softDeletes() {
    $type = "timestamp";
    $args = array("nullable" => true);
    $args = $this->parseOpts($args);

    $name = 'deleted_at';
    $this->addColumn($type, $name, $args); 
    return $this;
  }

  /**
   * String
   * 
   * Add a varchar column with the provided name
   *
   * @param string The column name
   * @param integer The length of the string
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function string($name, $length=255, $options=array()) {
    $type = "varchar($length)";
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Text
   * 
   * Add a text column with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function text($name, $options=array()) {
    $type = "text";
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Timestamp
   * 
   * Add a time column with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function time($name, $options=array()) {
    $type = "time";
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Timestamp
   * 
   * Add a timestamp with the provided name
   *
   * @param string The column name
   * @param array options such as nullable and default
   * @return ErosTable self for method chaining
   */
  public function timestamp($name, $options=array()) {
    $type = "timestamp";
    $args = $this->parseOpts($options);
    $this->addColumn($type, $name, $args);
    return $this;
  }

  /**
   * Timestamps
   * 
   * Add created_at and updated_at timestamps to the table
   *
   * @return ErosTable self for method chaining
   */
  public function timestamps() {
    $type = "timestamp";
    $opts = array("nullable" => false);
    $args = $this->parseOpts($opts);

    $name = 'updated_at';
    $this->addColumn($type, $name, $args); 

    $name = 'created_at';
    $this->addColumn($type, $name, $args); 

    return $this;
  }

  /**
   * Primary Composite
   * 
   * Add a primary key to the table
   *
   * @param string column name the key uses
   * @return ErosTable self for method chaining
   */
  public function primary($name) {
    $key = "PRIMARY KEY  ($name)";
    $this->keys["primary"][] = $key;
    return $this;
  }

  /**
   * Primary Composite
   * 
   * Add a primary key that is a composit of more than one column
   *
   * @param array column names the key uses
   * @return ErosTable self for method chaining
   */
  public function primaryComposite($name=array()) {
    $cols = join(',', $name);
    $key = "PRIMARY KEY ($cols)";
    $this->keys["primary"][] = $key;
    return $this;
  }

  /**
   * Unique
   * 
   * Add a unique index to the table
   *
   * @param string column name that should be unique
   * @return ErosTable self for method chaining
   */
  public function unique($name) {
    $key = "UNIQUE ($name)";
    $this->keys["unique"][] = $key;
    return $this;
  }

  /**
   * Index
   * 
   * Add a simple index to the table
   *
   * @param string column name the key
   * @return ErosTable self for method chaining
   */
  public function index($name) {
    $key = "INDEX ($name)";
    $this->keys["other"][] = $key;
    return $this;
  }

  /**
   * Foreign
   * 
   * Add a foreign key to the table
   *
   * @param string column name the key uses
   * @param string the table the key points to
   * @param string column name in foreign table to reference
   * @param options includes on delete and on update actions
   * @return ErosTable self for method chaining
   */
  public function foreign($name, $references, $on, $options=array()) {
    $key = "FOREIGN KEY ($name) REFERENCES $references($on)";
    return $this;
  }

  /**
   * Version
   * 
   * Set the version that this table has
   *
   * @param string Version of the table as described
   * @return ErosTable self for method chaining
   */
  public function version($version) {
    $this->version = $version;
    return $this;
  }

  public function hasColumn($name) {
    return in_array($name, $this->column_names);
  }

  /**
   * Current Version
   * 
   * Gets the current version in the database
   *
   * @return string current installed version number
   */
  private function currentVersion() {
    return get_option($this->table_name.'_table_version');
  }

  /**
   * Update version
   * 
   * Set the current version in the database
   */
  private function updateVersion() {
     update_option($this->table_name.'_table_version', $this->version);
  }

  /**
   * Requires Update
   * 
   * Compares the current version of the table the provided version of 
   * table and decides whether the table needs updating
   *
   * @return boolean Is an update required
   */
  private function requiresUpdate() {
    $ov = get_option($this->table_name.'_table_version');
    if(!$ov) {$ov = '0.0.0';}
    $nv = $this->version;
    return $ov != $nv;
  }

  /**
   * Parse Options
   * 
   * Takes a set of options and builds a sufficent array of arguments
   *
   * @params array Optional arguments for column
   * @return array Filled array of args
   */
  private function parseOpts($options) {
    $opts = array();

    if(isset($options['nullable'])) {
      if($options['nullable']) {
        $opts['not_null'] = 'NULL';
      } else {
        $opts['not_null'] = 'NOT NULL';
      }
    } else {
      $opts['not_null'] = 'NOT NULL';
    }
    
    if(isset($options['default'])) {
      $opts['default'] = "DEFAULT {$options['default']}";
    }

    if(isset($options['signed'])) {
      $opts['unsigned'] = 'UNSIGNED';
    } else {
      $opts['unsigned'] = '';
    }

    return $opts;
  }

  private function addColumn($type, $name, $args) {
    $this->column_names[] = $name;
    $col = "$name $type {$args['unsigned']} {$args['not_null']} {$args['default']} {$args['auto_incr']}";
    $this->columns[] = $col;
  }
}