<?php

/**
 * This settings page object which wraps the creating of wordpress settings pages
 * into a simple class based builder object. 
 * 
 * @since 0.0.1
 * @author Sam John Duke
 */
class ErosOptionsPage {

    private static $wp_settings_pages = array(
      "general",
      "writing",
      "reading",
      "discussion",
      "media",
      "privacy",
      "permalink"
    );

    //The parent to this page
    private $parent;

    // The name of the settings page
    private $page;

    // The page options array
    private $page_options;

    // Settings Sections to be added to the page
    private $sections;

    /**
     * Load the settings page and create a new one if necessary
     */
    public function __construct($parent, $page) {
      $this->parent = $parent;
      $this->page = $page;
      $this->page_options = array();
      $this->sections = array();

      // If this is not a page that is already defined by wordpress we need to create
      // it first and then add it to the admin section
      if(!in_array($this->page, self::$wp_settings_pages)) {
        add_action('admin_menu', array( &$this, 'create_page' ) );
      } 

      //Build the settings page after wordpress has been setup
      add_action('admin_menu', array( &$this, 'build' ) );
    }

    /*
     * Create a new page with required options
     */
    public function create_page() {
      add_submenu_page(
        $this->parent,
        $this->page_options['page_title'], // Page Title
        $this->page_options['menu_title'], // Menu Title
        $this->page_options['capability'], // required permission/capability
        $this->page, // menu slug
        array(&$this, 'render')
      );
    }

    /**
     * Parse the object_graph and insert all the relevant bits to form the settings page
     */
    public function build() {
      foreach($this->sections as $section) {
        $section->add_to_page($this->page);
      }
    }

    /**
     * Create a new section in this page, uses a closure to configure the section
     * 
     */
    public function add_section($name, $closure) {
      $section = new ErosSection($name);
      $closure($section);
      $this->sections[] = $section;
      return $this;
    }

    /**
     *
     */
    public function render() {
      if(!current_user_can('manage_options')) {
        wp_die(__('You do not have sufficient permissions to access this page.'));
      }

      include(sprintf("%s/forms/settings.php", dirname(__FILE__)));
    }

    public function page_title($page_title) {
      $this->page_options['page_title'] = $page_title;
      return $this;
    }

    public function menu_title($menu_title) {
      $this->page_options['menu_title'] = $menu_title;
      return $this;
    }

    public function capability($capability) {
      $this->page_options['capability'] = $capability;
      return $this;
    }

    public function description($des) {
      $this->page_options['description'] = $des;
      return $this;
    }
}
