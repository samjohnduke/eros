<?php
/**
 *
 */
class ErosMetaBox {

  private $id;
  private $title;
  private $description;
  private $form;
  private $context;
  private $priority;
  private $args;
  private $validate;
  private $page_name;

  public function __construct($id, $page) {
    $this->id = $id;
    $this->page_name = $page;
    $this->form = sprintf("%s/forms/meta_box.php", dirname(__FILE__));
    $this->context = 'normal';
    $this->priority = 'default';
    $this->args = array();

    add_action( 'save_post', array( $this, 'save' ) );
  }

  public function render($object, $args) {
    $object_id = $object->ID;
    $post_meta = get_post_meta($object_id, $this->id, true);
    if(!is_array($post_meta)) {
      $post_meta = array();
    }
    include($this->form);
  }

  public function title($str) {
    $this->title = $str;
    return $this;
  }

  public function form_file($str) {
    $this->form = $str;
    return $this;
  }

  public function context($str) {
    $this->context = $str;
    return $this;
  }

  public function priority($str) {
    $this->priority = $str;
    return $this;
  }

  public function validate($closure) {
    $this->validate = $closure;
    return $this;
  }

  public function description($str) {
    $this->description = $str;
    return $this;
  }

  public function build() {
    add_meta_box(
      $this->id,
      $this->title,
      array($this, 'render'),
      $this->page_name,
      $this->context,
      $this->priority,
      $this->args
    );
  }

  public function save($id) {
    if(!$this->checkIsSaveable($id)) {
      return;
    }
    
    // Sanitize user input.
    $data = $_POST[$this->id];

    // Update the meta field in the database.
    update_post_meta( $id, $this->id, $data );
  }

  private function checkIsSaveable($id) {
    if ( ! isset( $_POST['eros_nonce_'. $this->id] ) ) {
      return false;
    }

    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['eros_nonce_'. $this->id], 'eros_meta_box_'. $this->id ) ) {
      return false;
    }

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
      return false;
    }

    // Check the user's permissions.
    if ( !current_user_can( 'edit_post', $post->ID ) ) {
      return false;
    }

    return true;
  }

}