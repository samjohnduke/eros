<?php
/**
 *
 */
class CustomMetaBox {

  private $id;
  private $title;
  private $description;
  private $form;
  private $context;
  private $priority;
  private $args;
  private $validate;
  private $page_name;

  public function __construct($id, $page) {
    $this->id = $id;
    $this->page_name = $page;
    $this->form = sprintf("%s/forms/meta_box.php", dirname(__FILE__));
    $this->context = 'normal';
    $this->priority = 'default';
    $this->args = [];
  }

  public function render() {
    include($this->form);
  }

  public function title($str) {
    $this->title = $str;
    return $this;
  }

  public function form_file($str) {
    $this->form = $str;
    return $this;
  }

  public function context($str) {
    $this->context = $str;
    return $this;
  }

  public function priority($str) {
    $this->priority = $str;
    return $this;
  }

  public function validate($closure) {
    $this->validate = $closure;
    return $this;
  }

  public function description($str) {
    $this->description = $str;
    return $this;
  }

  public function build() {
    add_meta_box(
      $this->id,
      $this->title,
      array($this, 'render'),
      $this->page_name,
      $this->context,
      $this->priority,
      $this->args
    );
  }

  public function save($id, $content) {

  }

}