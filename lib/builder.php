<?php 
//fetch and include all the relevant builder files
require_relative([
  'lib/builder/eros_active_procedure.php',
  'lib/builder/eros_active_resource.php',
  'lib/builder/eros_type_builder.php',

  'lib/builder/eros_table.php',
  'lib/builder/eros_model.php',
  'lib/builder/eros_relationship.php',

  'lib/builder/eros_type.php',  
  'lib/builder/custom_taxonomy.php',
  'lib/builder/custom_meta_box.php',
    
  'lib/builder/eros_section.php',
  'lib/builder/eros_field.php',
  'lib/builder/eros_options_page.php',

  'lib/builder/fields/eros_text_field.php',
  'lib/builder/fields/eros_check_field.php',
  'lib/builder/fields/eros_dropdown_field.php',
  'lib/builder/fields/eros_color_field.php',
  'lib/builder/fields/eros_mediaupload_field.php',
  'lib/builder/fields/eros_multimediaupload_field.php',

  'lib/builder/eros_dashboard.php',
  'lib/builder/eros_dashboard_widget.php'
]);
