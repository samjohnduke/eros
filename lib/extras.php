<?php
/**
 * Clean up the_excerpt()
 */
function eros_excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'eros') . '</a>';
}
add_filter('excerpt_more', 'eros_excerpt_more');
