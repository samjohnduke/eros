<div class='page-header'>
  <div class='row'>
    <div class='columns medium-12'>

      <div class="page-title">
        <h1>
          <?php echo eros_title(); ?>
        </h1>
      </div>

    </div>
  </div>

  <?php 
  $header_type = get_option('header_type');
  if($header_type == 'color') {

  //FULL SCREEN BACKGROUND COLOR ?>

  <div class='bk-color-full' style='background-color: <?php echo get_option('bk_color_for_header'); ?>;'>
  </div>

  <?php } else if($header_type == 'image') { 
      $bkimg = wp_get_attachment_url(get_option('images_for_header')[0]);
    ?>

  <div class='bk-color-full' style='background: url(<?php echo $bkimg; ?>) no-repeat center center; 
    -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;'>
  </div>

  <?php } else if($header_type == 'image_slider') { ?>

  <div class='bk-color-full' >
      <div class='slider' data-eros-speed='1600'
                    data-eros-delay='3000'
                    data-eros-dots='true'
                    data-eros-keys='true'
                    data-eros-fluid='true'>
        <ul>
          <?php 
            $imgs = get_option('images_for_header');
            foreach($imgs as $img) {
              $i = wp_get_attachment_image_src($img, 'large')[0]; ?>
            <li>
              <div class='img' style='background: url(<?php echo $i; ?>) no-repeat center center; 
                        -webkit-background-size: cover; 
                        -moz-background-size: cover; 
                        -o-background-size: cover; 
                        background-size: cover;'>
            </li>
            <?php } ?>
        </ul>
      </div>
  </div>

  <?php } else if($header_type == 'video') { ?>



  <?php } ?>
</div>