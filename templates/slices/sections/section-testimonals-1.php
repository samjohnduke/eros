<section class='slider' data-eros-speed='1000'
                    data-eros-delay='1000'
                    data-eros-dots='true'
                    data-eros-keys='true'
                    data-eros-fluid='true' >
  <ul>
    <li>This is a slide.</li>
    <li>This is another slide.</li>
    <li>This is a final slide.</li>
  </ul>
</section>