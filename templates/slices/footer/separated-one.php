<footer class=" content-info" role="contentinfo">
  <div class='page-footer'>
    <div class="row">
      <div class='columns large-12'>
        <?php dynamic_sidebar('sidebar-footer'); ?>
      </div>
    </div>
  </div>
  <div class='separate'>
    <div class='row'>
      <div class=' columns'>
        <p>Copyright &copy; 2015 Eros</p>
      </div>
    </div>
  </div>
</footer>
