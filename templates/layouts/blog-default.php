<div class="row" role="document">
  <?php if (eros_display_sidebar()) : ?>
    <div class='columns small-12 medium-9'>
  <?php else : ?>
    <div class='columns small-12'>   
  <?php endif; ?>  

      <main class="main" role="main">
        <?php include eros_template_path(); ?>
      </main><!-- /.main -->
      
    </div>
    <?php if (eros_display_sidebar()) : ?>
      
      <div class='columns small-12 medium-3'>
        <aside class="sidebar" role="complementary">
          <?php include eros_sidebar_path(); ?>
        </aside><!-- /.sidebar -->
      </div>
    <?php endif; ?>

</div><!-- /.row -->