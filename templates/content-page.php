<div class='row'>
  <div class='columns medium-12'>
    <?php the_content(); ?>
    <?php wp_link_pages(array('before' => '<nav class="pagination">', 'after' => '</nav>')); ?>
  </div>
</div>
