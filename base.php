<?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?>>

  <!--[if lt IE 8]>
    <div class="alert alert-warning">
      <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'eros'); ?>
    </div>
  <![endif]-->

  <?php
    do_action('get_header');
    get_template_part('templates/slices/topbar/default');
  ?>

  <div class='root_div' role="document">
    <main class="main" role="main">
      <?php include eros_template_path(); ?>
    </main><!-- /.main -->

    <?php 
      $ftype = get_option('footer_type');
      $cols = get_option('footer_column_number');

      get_template_part('templates/slices/footer/'.$ftype, $cols); ?>

  </div><!-- /.row -->
  
  <?php wp_footer(); ?>

</body>
</html>
